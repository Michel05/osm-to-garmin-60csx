#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import os

def extrait_carte_europe_25m_terminal():

  root = Tk()
  root.title('GENERATION_EXTRAIT_CARTE_EUROPE 25m')
  termf = Frame(root, height=400, width=500)

  termf.pack(fill=BOTH, expand=YES)
  wid = termf.winfo_id()
  os.system('xterm -into %d -geometry 400x200 -T OSM_GARMIN_MAP_OUVERTURE_TERMINAL -bd red -bg grey -bw 5 -b 5 -sb /home/michel/osm/Génération_carte/Programme/prog_variable_25m.sh &' % wid)
  
  #root.destroy()
  root.mainloop()
  
 