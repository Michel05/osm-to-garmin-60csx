#!/bin/bash

jaune='\e[1;33m'
neutre='\e[0;m'

temps() { t=$(($2-$1)) ; printf "%d:%02d:%02d\n" $(($t/3600)) $((($t%3600)/60)) $(($t%60)) ; }
t1=$(date +%s)


###################################################################################
#Chargement de la base europe
#cd /home/michel/osm/base_osm/Datasets
#curl -# -O -L "http://download.geofabrik.de/openstreetmap/europe-latest.osm.pbf"
###################################################################################


#Saisie des variables
NORD=$null
SUD=$null
EST=$null
OUEST=$null

read -p 'Coordonnées NORD :' NORD
read -p 'Coordonnées SUD :' SUD
sudPlusGrandQueNord=$(echo $SUD'>'$NORD | bc -l) # vaut 1 si vrai et 0 si faux

while (( $sudPlusGrandQueNord ))
#while test $SUD -gt $SUD
do 
	echo " "
	echo -e " ${jaune}ERREUR DE SAISIE ${neutre}" && read -p 'Coordonnées NORD :' NORD && read -p 'Coordonnées SUD :' SUD
	sudPlusGrandQueNord=$(echo $SUD'>'$NORD | bc -l) # vaut 1 si vrai et 0 si faux

done

read -p 'Coordonnées OUEST :' OUEST
read -p 'Coordonnées EST :' EST

ouestPlusGrandQueEst=$(echo $OUEST'>'$EST | bc -l) # vaut 1 si vrai et 0 si faux
while (( $ouestPlusGrandQueEst ))
do

echo -e " ${jaune}ERREUR DE SAISIE ${neutre}" && read -p 'Coordonnées OUEST :' OUEST &&	read -p 'Coordonnées EST :' EST
ouestPlusGrandQueEst=$(echo $OUEST'>'$EST | bc -l) # vaut 1 si vrai et 0 si faux

done

heure_debut=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"

echo "Coordonnées NORD $NORD !"
echo "Coordonnées SUD $SUD !"
echo "Coordonnées OUEST $OUEST !"
echo "Coordonnées EST $EST !" 



#left=6.4903 right=6.5911 top=44.5188 bottom=44.4542

# PACA frontaliere left=4.5 right=7.8 top=45.3 bottom=42.9

################################################################
#1 Osmosis extraction zone de travail sur base europe compressée
#cd /home/michel/osm/base_osm/Datasets/ 
#/home/michel/osm/applis/osmosis-0.36/bin/osmosis --rx /home/michel/osm/base_osm/Datasets/europe-latest.osm.bz2 --bb left=$OUEST right=$EST top=$NORD bottom=$SUD --wx Extract_base_paca_front.osm
################################################################

#1 Osmosis extraction zone de travail sur base réduite
echo " "
echo -e "${jaune}extraction zone de travail ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/Base_osm 
/home/michel/osm/applis/osmosis-0.36/bin/osmosis --rx /home/michel/osm/Carte_Lauzet/Base_osm/base_paca_front.osm --bb left=$OUEST right=$EST top=$NORD bottom=$SUD --wx Extract_base_paca_front.osm

#Filtre de la base
echo " "
echo -e "${jaune}Filtrage de la base ${neutre}"
echo " "
osmosis --read-xml file=/home/michel/osm/Carte_Lauzet/Base_osm/Extract_base_paca_front.osm --tag-filter reject-nodes power=tower --tag-filter reject-nodes aeriaway=pylon --tag-filter reject-nodes internet_access=* --tag-filter reject-nodes railway=level_crossing --tag-filter reject-nodes amenity=parking --tag-filter reject-nodes parking=surface --tag-filter reject-nodes barrier=chain --tag-filter reject-nodes barrier=gate --tag-filter reject-nodes tourism=information --tag-filter reject-nodes landuse=cimetery --write-xml file=/home/michel/osm/Carte_Lauzet/Base_osm/tri_base.osm

#2 Récupération des données d'altitude
echo " "
echo -e "${jaune}Récupération des données d'altitude ${neutre}"
echo " "
cd ~/osm/applis/
mono /home/michel/osm/applis/Srtm2Osm/Srtm2Osm.exe -bounds1 $NORD $OUEST $SUD $EST  -step 25 -cat 500 100 -large -corrxy 0.0005 0.0006 -o /home/michel/osm/Carte_Lauzet/split_topo/topo_25.osm
#fait

#4 split de la carte SRTM
echo " "
echo -e "${jaune}split de la carte SRTM ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/split_topo
java -Xmx2G -jar /home/michel/osm/applis/splitter-r307/splitter.jar --keep-complete=false --mixed=true --mapid=72500001 /home/michel/osm/Carte_Lauzet/split_topo/topo_25.osm
#fait


#3 Split de la carte OSM
echo " "
echo -e "${jaune}Split de la carte OSM ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/split_osm
java -Xmx2G -jar /home/michel/osm/applis/splitter-r307/splitter.jar /home/michel/osm/Carte_Lauzet/Base_osm/tri_base.osm
#fait

#7 Création des 2 jeux de cartes osm et routage
echo " "
echo -e "${jaune}Création des 2 jeux de cartes osm et routage ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/split_osm
sed 's/mapname: 6324/mapname: 6100/g' template.args > routage.args
#fait

#5 Génération de la carte Garmin couche OSM
echo " "
echo -e "${jaune}Génération de la carte Garmin couche OSM ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/split_osm/
java -Xmx2G -jar /home/michel/osm/mkgmap_dernier/mkgmap-r2714/mkgmap.jar --gmapsupp --tdbfile --product-version="2014_01_19" --series-name="OSM map MA" --area-name="PACA Frontaliere" --family-id=42 --family-name="Fond carte OSM" --draw-priority=25 --add-pois-to-areas --style-file=/home/michel/osm/Carte_Lauzet/style/hc-map13/hc-map13.zip --remove-short-arcs --max-jobs=1 --latin1 --country-name="PACA" --country-abbr="FR" --generate-sea=extend-sea-sectors -c template.args --family-id=43 --family-name="Routage OSM" --route --draw-priority=5 --style-file=/home/michel/osm/Petrovsk/Style_Petrovsk/routage.zip --add-pois-to-areas --remove-short-arcs --max-jobs=1 --latin1 --country-name="PACA" --country-abbr="FR" -c routage.args
#  chemin style carte paca GPS--style-file=/home/michel/osm/Carte_Lauzet/style/hc-map4/hc-map8.zip


#5 Génération de la carte Garmin couche relief
echo " "
echo -e "${jaune}Génération de la carte Garmin couche relief ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/split_topo/
java -Xmx2G -jar /home/michel/osm/mkgmap_dernier/mkgmap-r2714/mkgmap.jar --gmapsupp --tdbfile --family-id=44 --family-name="Relief SRTM 25m" --draw-priority=50 --transparent --style-file=/home/michel/osm/mkgmap_dernier/mkgmap-r2714/examples/styles/default.zip --remove-short-arcs --max-jobs=1 --latin1 --series-name="relief 25m" --description="OSM" -c template.args /home/michel/osm/Petrovsk/Style_Petrovsk/44-contours-brown-l.TYP 
#fait

echo " "
echo -e "${jaune}Copie du fichier gmapsupp en gmapsupp_topo ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/split_topo/
mv gmapsupp.img gmapsupp_topo.img

echo " "
echo -e "${jaune}Copie du fichier gmapsupp en gmapsupp_osm ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/split_osm/
mv gmapsupp.img gmapsupp_osm.img

#Fusion des 2 gmapsupp
echo " "
echo -e "${jaune}Fusion des 2 fichiers ${neutre}"
echo " "
cd /home/michel/osm/Carte_Lauzet/resultat/
/home/michel/osm/applis/gmaptool/lgmt08159/gmt -j -o gmapsupp.img /home/michel/osm/Carte_Lauzet/split_osm/gmapsupp_osm.img /home/michel/osm/Carte_Lauzet/split_topo/gmapsupp_topo.img

heure_fin=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"
echo -e "${jaune}HEURE DE FIN : $heure_fin ${neutre}"
echo " "
t2=$(date +%s)
temps t1 t2


echo " "
echo -e "${jaune}Fin de la génération de la carte ${neutre}"
echo " "

echo " "

echo " Taper une touche"
read sortie
echo "SORTIE DU LOGICIEL"