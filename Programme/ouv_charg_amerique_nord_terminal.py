#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import os

def charg_amerique_nord_terminal():

  root = Tk()
  root.title('CHARGEMENT_DE_LA_BASE_AMERIQUE DU NORD')
  termf = Frame(root, height=400, width=500)

  termf.pack(fill=BOTH, expand=YES)
  wid = termf.winfo_id()
  os.system('xterm -into %d -geometry 400x200 -T CHARGEMENT_DE_LA_BASE_AMERIQUE DU NORD -bd red -bg grey -bw 5 -b 5  -sb bash /home/michel/osm/Génération_carte/Programme/Charg_base_amerique_nord.sh &' % wid)
  
  #root.destroy()
  root.mainloop()
  
 