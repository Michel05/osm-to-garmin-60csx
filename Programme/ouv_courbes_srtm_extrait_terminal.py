#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import os

def courbes_srtm_extrait_terminal():

  root = Tk()
  root.title('GENERATION_COURBES_NIVEAUX_SRTM')
  termf = Frame(root, height=400, width=500)

  termf.pack(fill=BOTH, expand=YES)
  wid = termf.winfo_id()
  os.system('xterm -into %d -geometry 400x200 -T GENERATION_COURBES_NIVEAUX_SRTM -bd red -bg grey -bw 5 -b 5  -sb bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_SRTM_Extrait.sh &' % wid)
  
  #root.destroy()
  root.mainloop()
  
 