#!/bin/bash

jaune='\e[1;33m'
neutre='\e[0;m'

temps() { t=$(($2-$1)) ; printf "%d:%02d:%02d\n" $(($t/3600)) $((($t%3600)/60)) $(($t%60)) ; }
t1=$(date +%s)


cd /home/michel/osm/Génération_carte/split_topo/
rm *.*

cd /home/michel/osm/Génération_carte/split_osm/
rm *.*

cd /home/michel/osm/Génération_carte/global/
rm *.*

###################################################################################
#Chargement de la base europe
#cd /home/michel/osm/base_osm/Datasets
#curl -# -O -L "http://download.geofabrik.de/openstreetmap/europe-latest.osm.pbf"
###################################################################################


#Saisie des variables
NORD=$null
SUD=$null
EST=$null
OUEST=$null

read -p 'Coordonnées NORD :' NORD
read -p 'Coordonnées SUD :' SUD
sudPlusGrandQueNord=$(echo $SUD'>'$NORD | bc -l) # vaut 1 si vrai et 0 si faux

while (( $sudPlusGrandQueNord ))
#while test $SUD -gt $SUD
do 
	echo " "
	echo -e " ${jaune}ERREUR DE SAISIE ${neutre}" && read -p 'Coordonnées NORD :' NORD && read -p 'Coordonnées SUD :' SUD
	sudPlusGrandQueNord=$(echo $SUD'>'$NORD | bc -l) # vaut 1 si vrai et 0 si faux

done

read -p 'Coordonnées OUEST :' OUEST
read -p 'Coordonnées EST :' EST

ouestPlusGrandQueEst=$(echo $OUEST'>'$EST | bc -l) # vaut 1 si vrai et 0 si faux
while (( $ouestPlusGrandQueEst ))
do

echo -e " ${jaune}ERREUR DE SAISIE ${neutre}" && read -p 'Coordonnées OUEST :' OUEST &&	read -p 'Coordonnées EST :' EST
ouestPlusGrandQueEst=$(echo $OUEST'>'$EST | bc -l) # vaut 1 si vrai et 0 si faux

done

heure_debut=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"

echo "Coordonnées NORD $NORD !"
echo "Coordonnées SUD $SUD !"
echo "Coordonnées OUEST $OUEST !"
echo "Coordonnées EST $EST !" 



################################################################
#1 Osmosis extraction zone de travail sur base europe compressée
cd /home/michel/osm/base_osm/Datasets/
osmosis --rb reunion-latest.osm.pbf --bb left=$OUEST right=$EST top=$NORD bottom=$SUD --tag-filter reject-nodes power=tower --tag-filter reject-nodes aeriaway=pylon --tag-filter reject-nodes internet_access=* --tag-filter reject-nodes railway=level_crossing --tag-filter reject-nodes amenity=parking --tag-filter reject-nodes parking=surface --tag-filter reject-nodes barrier=chain --tag-filter reject-nodes barrier=gate --tag-filter reject-nodes barrier=lift_gate --tag-filter reject-nodes tourism=information --wx Extract_base_reunion.osm



#2 Récupération des données d'altitude
echo " "
echo -e "${jaune}Récupération des données d'altitude ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/split_topo

phyghtmap -a $OUEST:$SUD:$EST:$NORD -o high_alps --source=view1,view3,srtm3 -s 25 -c 500,100 --corrx=0.0005 --corry=0.0005 --pbf


#3 Split de la carte OSM
echo " "
echo -e "${jaune}Split de la carte OSM ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/split_osm
java -Xmx2G -jar /home/michel/osm/applis/splitter-r307/splitter.jar /home/michel/osm/base_osm/Datasets/Extract_base_reunion.osm

#7 Création des 2 jeux de cartes osm et routage
echo " "
echo -e "${jaune}Création des 2 jeux de cartes osm et routage ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/split_osm
sed 's/mapname: 6324/mapname: 6100/g' template.args > routage.args
#fait

#copie des tuiles OSM et routage dans le réperoire Global
cd /home/michel/osm/Génération_carte/split_osm
cp *.* /home/michel/osm/Génération_carte/global

#copie des tuiles topo dans le réperoire Global
cd /home/michel/osm/Génération_carte/split_topo
cp *.* /home/michel/osm/Génération_carte/global

#8 Génération de la carte Garmin couche OSM
echo " "
echo -e "${jaune}Génération de la carte Garmin couche OSM ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/global
java -Xmx2G -jar /home/michel/osm/mkgmap_dernier/mkgmap-r2714/mkgmap.jar --gmapsupp --tdbfile --product-version="2014_04_01" --series-name="OSM PACA MA" --area-name="PACA Frontaliere" --family-id=42 --family-name="Fond carte OSM" --draw-priority=25 --add-pois-to-areas --style-file=/home/michel/osm/Génération_carte/style/perso/perso01.zip --remove-short-arcs --max-jobs=1 --latin1 --country-name="PACA" --country-abbr="FR" --generate-sea=extend-sea-sectors -c template.args --family-id=43 --family-name="Routage OSM" --route --draw-priority=5 --style-file=/home/michel/osm/Petrovsk/Style_Petrovsk/routage.zip --add-pois-to-areas --remove-short-arcs --max-jobs=1 --latin1 --country-name="PACA" --country-abbr="FR" -c routage.args --family-id=44 --mapname=72500001 --family-name="Relief SRTM 25m" --draw-priority=50 --transparent --style-file=/home/michel/osm/mkgmap_dernier/mkgmap-r2714/examples/styles/default.zip --remove-short-arcs --max-jobs=1 --latin1 --series-name="relief 25m" --description="OSM" *.pbf /home/michel/osm/Génération_carte/TYP/I0000186.TYP /home/michel/osm/Génération_carte/TYP/44-contours.TYP 


heure_fin=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"
echo -e "${jaune}HEURE DE FIN : $heure_fin ${neutre}"
echo " "
t2=$(date +%s)
temps t1 t2


echo " "
echo -e "${jaune}Fin de la génération de la carte ${neutre}"
echo " "
