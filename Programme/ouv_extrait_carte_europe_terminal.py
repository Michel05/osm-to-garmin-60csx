#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import os

def extrait_carte_europe_terminal():

  root = Tk()
  root.title('GENERATION_EXTRAIT_CARTE_EUROPE')
  termf = Frame(root, height=400, width=500)

  termf.pack(fill=BOTH, expand=YES)
  wid = termf.winfo_id()
  os.system('xterm -into %d -geometry 400x200 -T GENERATION_EXTRAIT_CARTE_EUROPE -bd red -bg grey -bw 5 -b 5  -sb bash /home/michel/osm/Génération_carte/Programme/prog_variable.sh &' % wid)
  
  #root.destroy()
  root.mainloop()
  
 