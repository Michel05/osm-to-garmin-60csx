#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import os

def gene_carte_paca_global_phygtmap_10m_terminal():

  root = Tk()
  root.title('GENERATION_CARTE_PACA 10M')
  termf = Frame(root, height=400, width=500)

  termf.pack(fill=BOTH, expand=YES)
  wid = termf.winfo_id()
  os.system('xterm -into %d -geometry 400x200 -T OSM_GARMIN_MAP_OUVERTURE_TERMINAL -bd red -bg grey -bw 5 -b 5 -sb /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_phygtmap_10m.sh &' % wid)
  
  #root.destroy()
  root.mainloop()
  
 