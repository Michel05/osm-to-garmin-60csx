#!/bin/sh

jaune='\e[1;33m'
neutre='\e[0;m'

temps() { t=$(($2-$1)) ; printf "%d:%02d:%02d\n" $(($t/3600)) $((($t%3600)/60)) $(($t%60)) ; }
t1=$(date +%s)

heure_debut=$(date +%H%M)   
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"

echo " "
echo -e "${jaune}Mise à jour de la base Europe ${neutre}"
echo " " 

zero=0
cd /home/michel/osm/base_osm/Datasets


osmupdate /home/michel/osm/base_osm/Datasets/maj_europe.osm.pbf /home/michel/osm/base_osm/Datasets/temp.osm.pbf -B=/home/michel/osm/base_osm/Datasets/europe.poly --max-merge=7 > osmosis.log 2>&1
retour=$?

if [ "$retour" -eq "$zero" ]	
then
	mv temp.osm.pbf maj_europe.osm.pbf
	echo "Le traitement du fichier par osmosis a réussi" 
else
	echo "Le traitement du fichier par osmosis n'a pas réussi"
	rm temp.osm.pbf
fi

heure_fin=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"
echo -e "${jaune}HEURE DE FIN : $heure_fin ${neutre}"
echo " "
t2=$(date +%s)
temps t1 t2

cd /home/michel/osm/Génération_carte/Programme/statistiques

temps t1 t2 >maj_base_europe

echo " "
echo " Taper une touche"
read sortie
echo "SORTIE DU LOGICIEL"