#!/bin/bash

jaune='\e[1;33m'
neutre='\e[0;m'

temps() { t=$(($2-$1)) ; printf "%d:%02d:%02d\n" $(($t/3600)) $((($t%3600)/60)) $(($t%60)) ; }
t1=$(date +%s)

heure_debut=$(date +%H%M)   
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"

echo " "
echo -e "${jaune}Chargement de la base Afrique ${neutre}"
echo " "

#Chargement de la base europe
cd /home/michel/osm/base_osm/Datasets
if test -f africa-latest.osm.pbf ; then
rm africa-latest.osm.pbf
wget http://download.geofabrik.de/openstreetmap/africa-latest.osm.pbf
#curl -# -O -L "http://download.geofabrik.de/openstreetmap/europe-latest.osm.pbf" 

heure_fin=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"
echo -e "${jaune}HEURE DE FIN : $heure_fin ${neutre}"
echo " "
t2=$(date +%s)
temps t1 t2

cd /home/michel/osm/Génération_carte/Programme/statistiques

temps t1 t2 >charg_base_afrique

echo " "
echo -e "${jaune}Fin du chargement de la base Afrique ${neutre}"
echo -e "${jaune}Répertoire /home/michel/osm/base_osm/Datasets/ ${neutre}"
echo " "

echo " Taper une touche"
read sortie
echo "SORTIE DU LOGICIEL"