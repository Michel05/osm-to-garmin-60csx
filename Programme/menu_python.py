#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
from ouv_surv_process                                    import surv_process
from ouv_terminal                                        import terminal
from ouv_charg_afrique_terminal                          import charg_afrique_terminal
from ouv_charg_antartique_terminal                       import charg_antartique_terminal
from ouv_charg_asie_terminal                             import charg_asie_terminal
from ouv_charg_australie_terminal                        import charg_australie_terminal
from ouv_charg_amerique_nord_terminal                    import charg_amerique_nord_terminal
from ouv_charg_amerique_centrale_terminal                import charg_amerique_centrale_terminal
from ouv_charg_amerique_sud_terminal                     import charg_amerique_sud_terminal
from ouv_charg_europe_terminal                           import charg_europe_terminal
from ouv_charg_paca_terminal                             import charg_paca_terminal
from ouv_maj_europe_terminal                             import maj_europe_terminal
from ouv_courbes_phygtmap_paca_terminal                  import courbes_phygtmap_paca_terminal
from ouv_courbes_srtm_paca_terminal                      import courbes_srtm_paca_terminal
#from ouv_courbes_phygtmap_extrait_terminal              import courbes_phygtmap_extrait_terminal
#from ouv_courbes_srtm_extrait_terminal                  import courbes_srtm_extrait_terminal
#from ouv_gene_carte_paca_niveau_terminal                import gene_carte_paca__niveau_terminal
#from ouv_gene_carte_paca_terminal                       import gene_carte_paca_terminal
#from ouv_generation_pbf_paca_global_srtm_terminal       import generation_pbf_paca_global_srtm_terminal
from ouv_gene_carte_paca_global_phygtmap_10m_terminal    import gene_carte_paca_global_phygtmap_10m_terminal
from ouv_gene_carte_paca_global_phygtmap_25m_terminal    import gene_carte_paca_global_phygtmap_25m_terminal
from ouv_maj_carte_paca_global_phygtmap_10m_terminal     import maj_carte_paca_global_phygtmap_10m_terminal
from ouv_maj_carte_paca_global_phygtmap_25m_terminal     import maj_carte_paca_global_phygtmap_25m_terminal
from ouv_extrait_carte_europe_terminal                   import extrait_carte_europe_terminal
from ouv_extrait_carte_europe_10m_terminal               import extrait_carte_europe_10m_terminal
from ouv_extrait_carte_europe_25m_terminal               import extrait_carte_europe_25m_terminal

import subprocess

#================================================================
def ouv_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Essai/ouv_terminal.sh", shell=True)
  terminal()

#Fenetre de surveillance du process
def ouv_surv_process():
  surv_process()
  
#Ouverture de page web
def ouv_web():
  subprocess.call("/home/michel/osm/Génération_carte/Programme/Essai/ouv_web.sh", shell=True)
  
#================================================================
def ouv_charg_afrique_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf
  charg_afrique_terminal()

def ouv_charg_antartique_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf
  charg_antartique_terminal()

def ouv_charg_asie_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf
  charg_asie_terminal()

def ouv_charg_australie_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf
  charg_australie_terminal()

def ouv_charg_amerique_nord_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf
  charg_amerique_nord_terminal()

def ouv_charg_amerique_centrale_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf
  charg_amerique_centrale_terminal()
  
def ouv_charg_amerique_sud_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf
  charg_amerique_sud_terminal()  

def ouv_charg_europe_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf
  charg_europe_terminal()  

def ouv_charg_paca_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_Base_PACA.sh", shell=True)
  charg_paca_terminal()

#================================================================

def ouv_maj_europe_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/MaJ_Europe.sh", shell=True)
  maj_europe_terminal()
  

#=====================================================================

def ouv_courbes_phygtmap_paca_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_phygtmap_PACA.sh", shell=True) 
  courbes_phygtmap_paca_terminal()

def ouv_courbes_srtm_paca_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_SRTM_PACA.sh", shell=True)
  Courbes_srtm_paca_terminal()
 
def ouv_courbes_phygtmap_extrait_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_phygtmap_Extrait.sh", shell=True)
  courbes_phygtmap_extrait_terminal()

def ouv_courbes_srtm_extrait_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_SRTM_Extrait.sh", shell=True)
  courbes_srtm_extrait_terminal()
  
  
#============================================================================
 
def ouv_gene_carte_paca_niveau_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_niveau.sh", shell=True) 
  gene_carte_paca__niveau_terminal()

def ouv_gene_carte_paca_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA.sh", shell=True)
  gene_carte_paca_terminal()
  
def ouv_gene_carte_paca_global_srtm_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_SRTM.sh", shell=True)
  gene_carte_paca_global_srtm_terminal()

def ouv_gene_carte_paca_global_phygtmap_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_phygtmap.sh", shell=True)
  gene_carte_paca_global_phygtmap_terminal()  
 
def ouv_gene_carte_paca_global_phygtmap_10m_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_phygtmap.sh", shell=True)
  gene_carte_paca_global_phygtmap_10m_terminal()

def ouv_gene_carte_paca_global_phygtmap_25m_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_phygtmap.sh", shell=True)
  gene_carte_paca_global_phygtmap_25m_terminal()

def ouv_maj_carte_paca_global_phygtmap_10m_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_phygtmap.sh", shell=True)
  maj_carte_paca_global_phygtmap_10m_terminal()

def ouv_maj_carte_paca_global_phygtmap_25m_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_phygtmap.sh", shell=True)
  maj_carte_paca_global_phygtmap_25m_terminal()

def ouv_extrait_carte_europe_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/prog_variable.sh", shell=True)
  extrait_carte_europe_terminal()

def ouv_extrait_carte_europe_10m_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/prog_variable.sh", shell=True)
  extrait_carte_europe_10m_terminal()

def ouv_extrait_carte_europe_25m_terminal():
  #subprocess.call("bash /home/michel/osm/Génération_carte/Programme/prog_variable.sh", shell=True)
  extrait_carte_europe_25m_terminal()

def Extrait_carte_Monde():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/prog_variable_Phygtmap.sh", shell=True)

#===========================================================================================
def Compil_TYP():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/compilTYP.sh", shell=True)
#===========================================================================================
       
def About():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Hello.sh", shell=True)

	   
   
root = Tk()
root.title('OSM GARMIN MAP')
#root.geometry('400x100+400+400')
root.geometry('800x800+800+800')

#logo_osm=PhotoImage(file="/home/michel/osm/Génération_carte/Programme/Logo/Logo_OSM.ppm")
logo_osm=PhotoImage(file="/home/michel/osm/Génération_carte/Programme/Logo/Logo_carte.ppm")
w1 = Label(root, image=logo_osm, anchor=W)
w1.logo_osm = logo_osm
w1.pack()

menu = Menu(root)
root.config(menu=menu)

charg_menu = Menu(menu)
menu.add_cascade(label="Chargement base", menu=charg_menu)
charg_menu.add_command(label="Afrique", command=ouv_charg_afrique_terminal)
charg_menu.add_command(label="Antartique", command=ouv_charg_antartique_terminal)
charg_menu.add_command(label="Asie", command=ouv_charg_asie_terminal)
charg_menu.add_command(label="Australie et Oceanie", command=ouv_charg_australie_terminal)
charg_menu.add_command(label="Amerique du Nord", command=ouv_charg_amerique_nord_terminal)
charg_menu.add_command(label="Amerique centrale", command=ouv_charg_amerique_centrale_terminal)
charg_menu.add_command(label="Ameriquedu Sud", command=ouv_charg_amerique_sud_terminal)
charg_menu.add_separator()
charg_menu.add_command(label="Europe", command=ouv_charg_europe_terminal)
charg_menu.add_command(label="PACA...", command=ouv_charg_paca_terminal)
charg_menu.add_separator()
charg_menu.add_command(label="Mise à jour de la base Europe...", command=ouv_maj_europe_terminal)
charg_menu.add_separator()
charg_menu.add_command(label="Exit", command=root.quit)

crea_courbes_menu = Menu(menu)
menu.add_cascade(label="Génération des courbes de niveaux", menu=crea_courbes_menu)
charg_menu.add_separator()
crea_courbes_menu.add_command(label="Phygtmap PACA", command=ouv_courbes_phygtmap_paca_terminal)
crea_courbes_menu.add_command(label="SRTM PACA", command=root.quit)
crea_courbes_menu.add_separator()
crea_courbes_menu.add_command(label="Extrait Phygtmap", command=root.quit)
crea_courbes_menu.add_command(label="Extrait SRTM", command=root.quit)
crea_courbes_menu.add_separator()
crea_courbes_menu.add_command(label="Exit", command=root.quit)

crea_menu = Menu(menu)
menu.add_cascade(label="Génération Carte GPS", menu=crea_menu)
crea_menu.add_command(label="1er Génération Carte PACA SRTM", command=ouv_gene_carte_paca_global_srtm_terminal) #Génération des courbes de niveaux avec la carte
crea_menu.add_command(label="1er Génération Carte PACA phygtmap 10M", command=ouv_gene_carte_paca_global_phygtmap_10m_terminal)
crea_menu.add_command(label="1er Génération Carte PACA phygtmap 25M", command=ouv_gene_carte_paca_global_phygtmap_25m_terminal)
crea_menu.add_separator()
crea_menu.add_command(label="Mise à jour Carte PACA 10M", command=ouv_maj_carte_paca_global_phygtmap_10m_terminal)
crea_menu.add_command(label="Mise à jour Carte PACA 25M", command=ouv_maj_carte_paca_global_phygtmap_25m_terminal)
crea_menu.add_separator()
crea_menu.add_command(label="Extrait de Carte Europe 10M", command=ouv_extrait_carte_europe_10m_terminal)
crea_menu.add_command(label="Extrait de Carte Europe 25M", command=ouv_extrait_carte_europe_25m_terminal)
crea_menu.add_separator()
crea_menu.add_command(label="Ouverture terminal", command=ouv_terminal)
crea_menu.add_separator()
crea_menu.add_command(label="Exit", command=root.quit)

divers = Menu(menu)
menu.add_cascade(label="Divers", menu=divers)
divers.add_command(label="Compilation fichier.txt en TYP", command=Compil_TYP)
divers.add_separator()
divers.add_command(label="Ouverture page web coordonnées géographique", command=ouv_web)
divers.add_separator()
divers.add_command(label="surveillance process", command=ouv_surv_process)
divers.add_command(label="Exit", command=root.quit)


helpmenu = Menu(menu)
menu.add_cascade(label="Aide", menu=helpmenu)
helpmenu.add_command(label="A propos de ...", command=About)

mainloop()
