#!/bin/bash

echo "+++++++++++++++++++++++++++++++++++++++++++++++++ "
echo " PROGRAMME DE GENERATION DES CARTES GPS GARMIN "
echo "++++++++++++++++++++++++++++++++++++++++++++++++++"

select CHOIX in "Chargement de la base europe" \
		"Chargement de la base PACA" \
		"Génération de la carte PACA frontaliere" \
		"Génération d'un extrait carte PACA" \
		"Compilation d'un TYP" \
		"Ajout d'un TYP" \
		"Quitter"
	do
		case $REPLY in
			1) echo "$CHOIX --> $REPLY" && cd /home/michel/osm/base_osm/Datasets && curl -# -O -L "http://download.geofabrik.de/openstreetmap/europe-latest.osm.bz2";;
			2) echo "$CHOIX --> $REPLY" && cd /home/michel/osm/base_osm/Datasets && curl -# -O -L "http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.bz2";;
			3) echo "$CHOIX --> $REPLY" && /home/michel/osm/Génération_carte/Programme/Generation__bz2_PACA.sh;;
			4) echo "$CHOIX --> $REPLY" && /home/michel/osm/Génération_carte/Programme/prog_variable.sh;;
			5) echo "$CHOIX --> $REPLY"&& /home/michel/osm/Génération_carte/Programme/compilTYP.sh;;
			6) echo "$CHOIX --> $REPLY";;
			7) echo "$CHOIX --> $REPLY" && exit;;
			*) echo "Ce choix n'est pas disponible";;
		esac
	done

#Ajouter la destination des fichiers