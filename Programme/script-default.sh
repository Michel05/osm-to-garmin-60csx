#!/bin/bash
maxnodes=1200000
maxareas=2048
name=Auvergne
abbr=AUV
contour=20
date="`date +%d%b%y`"
# Découpage
java -ea -Xms1G -Xmx2G -jar ../applis/splitter.jar --max-nodes=$maxnodes --write-kml=$name".kml" --output=o5m --max-areas=$maxareas ../base_$name/base.osm > splitter.log 2> splitter.log
# Génération de la carte
java -ea -Xms1G -Xmx2G -jar ../applis/mkgmap.jar --gmapsupp \
--max-jobs=3 --latin1 \
--bounds=../bounds_20140311.zip \
--route \
--index --tdbfile --housenumbers \
--show-profiles=1 \
--location-autofill=is_in,nearest \
--name-tag-list=name:fr,int_name,name \
--adjust-turn-headings \
--process-destination --process-exits \
--remove-short-arcs \
--x-add-indirect-links \
--list-styles --verbose \
--check-styles --style-file=../styles/default/ \
--family-id=4 --family-name="Carte OSM" \
--draw-priority=25 \
--reduce-point-density=4 \
--reduce-point-density-polygon=8 \
--polygon-size-limits="24:12, 18:10, 16:8, 14:4, 12:2, 11:0" \
--preserve-element-order \
--add-pois-to-lines \
--add-pois-to-areas \
--adjust-turn-headings \
--report-similar-arcs \
--link-pois-to-ways \
--drive-on-right --check-roundabouts --check-roundabout-flares \
--country-name="$name" --country-abbr="$abbr" \
--generate-sea=multipolygon,floodblocker -c template.args \
../styles/default/default.TYP \
--family-id=13 --family-name="Relief SRTM $contour'm'" \
--draw-priority=30 --transparent \
--series-name="Topo $contour Relief" \
--description="$name OSM" \
gmapsupp1.img

mv ./gmapsupp.img ./$name-$date-gmapsupp.img
