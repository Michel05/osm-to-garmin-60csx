#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import os

def courbes_phygtmap_extrait_terminal():

  root = Tk()
  root.title('GENERATION_COURBE_NIVEAU_PHYGTMAP_EXTRAIT')
  termf = Frame(root, height=400, width=500)

  termf.pack(fill=BOTH, expand=YES)
  wid = termf.winfo_id()
  os.system('xterm -into %d -geometry 400x200 -T GENERATION_COURBE_NIVEAU_PHYGTMAP_EXTRAIT -bd red -bg grey -bw 5 -b 5  -sb bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_phygtmap_Extrait.sh &' % wid)
  
  #root.destroy()
  root.mainloop()
  
 