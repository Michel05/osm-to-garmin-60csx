#!/bin/sh

#jaune='\e[1;33m'
#neutre='\e[0;m'

variable=`kdialog --title " CREATION DE CARTES GARMIN sur base OPEN STREET MAP" --menu "CHOIX:" A "Chargement de la base europe" B "Chargement de la base PACA" C "Génération de la carte PACA avec chargement de la base" D "Génération d'une portion de carte europe avec chargement de la base" E "Compilation d'un fichier TYP" F "Ajout d'un fichier TYP";`; 

if [ "$?" = 0 ]; then
	if [ "$variable" = A ]; then
		echo "Chargement de la base europe" && cd /home/michel/osm/base_osm/Datasets && curl -# -O -L "http://download.geofabrik.de/openstreetmap/europe-latest.osm.pbf"
	elif [ "$variable" = B ]; then
		echo "Chargement de la base PACA"&& cd /home/michel/osm/base_osm/Datasets && curl -# -O -L "http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf";
	elif [ "$variable" = C ]; then
		echo "Génération de la carte PACA"&& /home/michel/osm/Génération_carte/Programme/Generation__pbf_PACA.sh;
	elif [ "$variable" = D ]; then
		echo "Génération de la carte réduite"&& /home/michel/osm/Génération_carte/Programme/prog_variable.sh;
	elif [ "$variable" = E ]; then
		echo "Compilation d'un fichier TYP"&& /home/michel/osm/Génération_carte/Programme/compilTYP.sh;
	elif [ "$variable" = F ]; then
		echo "Ajout d'un fichier TYP"&& /home/michel/osm/Génération_carte/Programme/compilTYP.sh;
	else
		echo "PLEASE CHOOSE A COLOR";
	fi;
elif [ "$?" = 1 ]; then
	echo "SORTIE DU LOGICIEL";
else
	echo "ERREUR";
fi;



