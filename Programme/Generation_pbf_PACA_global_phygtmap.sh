#!/bin/bash

jaune='\e[1;33m'
neutre='\e[0;m'

temps() { t=$(($2-$1)) ; printf "%d:%02d:%02d\n" $(($t/3600)) $((($t%3600)/60)) $(($t%60)) ; }
t1=$(date +%s)

heure_debut=$(date +%H%M)   
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"

#echo " "
#echo -e "${jaune}Chargement de la base Europe ${neutre}"
#echo " "
###################################################################################
#Chargement de la base europe
#cd /home/michel/osm/base_osm/Datasets
#if test -f europe-latest.osm.pbf ; then
#rm europe-latest.osm.pbf
#wget http://download.geofabrik.de/openstreetmap/europe-latest.osm.pbf
#curl -# -O -L "http://download.geofabrik.de/openstreetmap/europe-latest.osm.pbf"
###################################################################################


 
#heure_debut=$(date +%H%M)   
#echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"


#left=6.4903 right=6.5911 top=44.5188 bottom=44.4542

# PACA frontaliere left=4.5 right=7.8 top=45.3 bottom=42.9

################################################################
#1 Osmosis extraction zone de travail sur base europe compressée
echo " "
echo -e "${jaune}extraction de la base ${neutre}" 
echo " "
cd /home/michel/osm/base_osm/Datasets/ 
osmosis --rb maj_europe.osm.pbf --bb left=4.5 right=7.8 top=45.3 bottom=42.9 --tag-filter reject-nodes power=tower --tag-filter reject-nodes aeriaway=pylon --tag-filter reject-nodes internet_access=* --tag-filter reject-nodes railway=level_crossing --tag-filter reject-nodes amenity=parking --tag-filter reject-nodes parking=surface --tag-filter reject-nodes barrier=chain --tag-filter reject-nodes barrier=gate --tag-filter reject-nodes barrier=lift_gate --tag-filter reject-nodes tourism=information --wx Extract_base_paca_front.osm
################################################################



########################################################################
#2 Récupération des données d'altitude
#echo " "
#echo -e "${jaune}Récupération des données d'altitude ${neutre}"
#echo " "
#cd /home/michel/osm/Génération_carte/topo_phygtmap_PACA

#phyghtmap -a 4.5:42.9:7.8:45.3 -o high_alps --source=view1,view3,srtm3 -s 25 -c 500,100 --corrx=0.0005 --corry=0.0005 --pbf

#4 Génération de la carte Garmin couche relief
#echo " "
#echo -e "${jaune}Génération de la carte Garmin couche relief ${neutre}"
#echo " "
#cd /home/michel/osm/Génération_carte/split_topo_PACA
#java -Xmx2G -jar /home/michel/osm/mkgmap_dernier/mkgmap-r2714/mkgmap.jar --gmapsupp --tdbfile --family-id=44 --family-name="Relief SRTM 25m" --draw-priority=50 --transparent --style-file=/home/michel/osm/mkgmap_dernier/mkgmap-r2714/examples/styles/default.zip --remove-short-arcs --max-jobs=1 --latin1 --series-name="relief 25m" --description="OSM" -c template.args /home/michel/osm/Petrovsk/Style_Petrovsk/44-contours-brown-l.TYP 
#fait

#5 copie gmapsupp en gmapsupp_topo
#echo " "
#echo -e "${jaune}Copie du fichier gmapsupp en gmapsupp_topo ${neutre}"
#echo " "
#cd /home/michel/osm/Génération_carte/split_topo_PACA
#mv gmapsupp.img gmapsupp_topo.img




#6 Split de la carte OSM
echo " "
echo -e "${jaune}Split de la carte OSM ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/split_osm_PACA
java -Xmx4G -jar /home/michel/osm/applis/splitter-r307/splitter.jar /home/michel/osm/base_osm/Datasets/Extract_base_paca_front.osm
#fait

#7 Création des 2 jeux de cartes osm et routage
echo " "
echo -e "${jaune}Création des 2 jeux de cartes osm et routage ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/split_osm_PACA
sed 's/mapname: 6324/mapname: 6100/g' template.args > routage.args
#fait

#copie des tuiles OSM et routage dans le réperoire Global_PACA
cd /home/michel/osm/Génération_carte/split_osm_PACA
cp *.* /home/michel/osm/Génération_carte/global_PACA

#copie des tuiles topo dans le réperoire Global_PACA
#cd /home/michel/osm/Génération_carte/topo_phygtmap_PACA
#cp *.* /home/michel/osm/Génération_carte/global_PACA

#8 Génération de la carte Garmin couche OSM
echo " "
echo -e "${jaune}Génération de la carte Garmin couche OSM ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/global_PACA
java -Xmx4G -jar /home/michel/osm/mkgmap_dernier/mkgmap-r2714/mkgmap.jar --gmapsupp --tdbfile --product-version="2014_11_14" --series-name="OSM PACA MA" --area-name="PACA Frontaliere" --family-id=6224 --family-name="Fond carte OSM" --draw-priority=25 --add-pois-to-areas --style-file=/home/michel/osm/Génération_carte/style/hc-map14/hc-perso141221.zip --remove-short-arcs --max-jobs=2 --latin1 --country-name="PACA" --country-abbr="FR" --generate-sea=extend-sea-sectors -c template.args --family-id=43 --family-name="Routage OSM" --route --draw-priority=5 --style-file=/home/michel/osm/EXEMPLES/Petrovsk/Style_Petrovsk/routage.zip --add-pois-to-areas --remove-short-arcs --max-jobs=2 --latin1 --country-name="PACA" --country-abbr="FR" -c routage.args --family-id=44 --mapname=72500001 --family-name="Relief SRTM 25m" --draw-priority=50 --transparent --style-file=/home/michel/osm/mkgmap_dernier/mkgmap-r2714/examples/styles/default.zip --remove-short-arcs --max-jobs=2 --latin1 --series-name="relief 25m" --description="OSM" *.pbf /home/michel/osm/Génération_carte/TYP/186.typ #/home/michel/osm/Génération_carte/TYP/44-contours.TYP

#/home/michel/osm/Génération_carte/style/hc-map14/hc-map18.zip

heure_fin=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"
echo -e "${jaune}HEURE DE FIN : $heure_fin ${neutre}"
echo " "
t2=$(date +%s)
temps t1 t2


echo " "
echo -e "${jaune}Fin de la génération de la carte PACA globale ${neutre}"
echo -e "${jaune}Répertoire /home/michel/osm/Génération_carte/global_PACA ${neutre}"
echo " " 

echo " "

echo " Taper une touche"
read sortie
echo "SORTIE DU LOGICIEL"
