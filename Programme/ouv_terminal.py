#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import os


def terminal():

  root = Tk()
  root.title('OSM GARMIN MAP OUVERTURE TERMINAL')
  termf = Frame(root, height=400, width=500)

  termf.pack(fill=BOTH, expand=YES)
  wid = termf.winfo_id()
    
  os.system('xterm -into %d -geometry 400x200 -T OSM_GARMIN_MAP_OUVERTURE_TERMINAL -bd red -bg grey -bw 5 -b 5 -sb /home/michel/osm/Génération_carte/Programme/Hello.sh &' % wid)
 
  
  root.mainloop()
  
 