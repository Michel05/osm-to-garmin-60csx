#!/bin/bash

jaune='\e[1;33m'
neutre='\e[0;m'

temps() { t=$(($2-$1)) ; printf "%d:%02d:%02d\n" $(($t/3600)) $((($t%3600)/60)) $(($t%60)) ; }
t1=$(date +%s)

heure_debut=$(date +%H%M)   
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"

echo " "
echo -e "${jaune}Création des courbes de niveaux PACA ${neutre}"
echo " "

cd /home/michel/osm/Génération_carte/topo_phygtmap_PACA

phyghtmap -a 4.5:42.9:7.8:45.3 -o high_alps --source=view1,view3,srtm3 -s 10 -c 50,10 --corrx=-0.0003 --corry=0.00005 --pbf



java -Xmx2G -jar /home/michel/osm/mkgmap_dernier/mkgmap-r2714/mkgmap.jar --gmapsupp --tdbfile --family-id=44 --mapname=72500001 --family-name="Relief 10m" --draw-priority=50 --transparent --style-file=/home/michel/osm/mkgmap_dernier/mkgmap-r2714/examples/styles/default.zip --series-name=srtm --overview-mapname=srtm --area-name=PACA --max-jobs=1 *.pbf


heure_fin=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"
echo -e "${jaune}HEURE DE FIN : $heure_fin ${neutre}"
echo " "
t2=$(date +%s)
temps t1 t2


echo " "
echo -e "${jaune}Fin de la génération des courbes de niveaux (phyghtmap) PACA ${neutre}"
echo -e "${jaune}Répertoire /home/michel/osm/Génération_carte/topo_phygtmap_PACA/ ${neutre}"
echo " "

echo " "

echo " Taper une touche"
read sortie
echo "SORTIE DU LOGICIEL"