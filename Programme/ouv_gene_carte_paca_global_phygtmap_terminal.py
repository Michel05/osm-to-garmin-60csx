#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import os

def gene_carte_paca_global_phygtmap_terminal():

  root = Tk()
  root.title('GENERATION_CARTE_PACA')
  termf = Frame(root, height=400, width=500)

  termf.pack(fill=BOTH, expand=YES)
  wid = termf.winfo_id()
  os.system('xterm -into %d -geometry 400x200 -T GENERATION_CARTE_PACA -bd red -bg grey -bw 5 -b 5  -sb bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_phygtmap.sh &' % wid)
  
  #root.destroy()
  root.mainloop()
  
 