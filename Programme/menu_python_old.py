#!/usr/bin/python3
# -*- coding: utf-8 -*-

ffrom tkinter import *
from ouv_terminal import terminal
from ouv_charg_europe_terminal                           import charg_europe_terminal
from ouv_charg_paca_terminal                             import charg_paca_terminal
from ouv_maj_europe_terminal                             import maj_europe_terminal
from ouv_courbes_phygtmap_paca_terminal                  import courbes_phygtmap_paca_terminal
from ouv_courbes_srtm_paca_terminal                      import courbes_srtm_paca_terminal
#from ouv_courbes_phygtmap_extrait_terminal              import courbes_phygtmap_extrait_terminal
#from ouv_courbes_srtm_extrait_terminal                  import courbes_srtm_extrait_terminal
#from ouv_gene_carte_paca_niveau_terminal                import gene_carte_paca__niveau_terminal
#from ouv_gene_carte_paca_terminal                       import gene_carte_paca_terminal
#from ouv_generation_pbf_paca_global_srtm_terminal       import generation_pbf_paca_global_srtm_terminal
#from ouv_generation_pbf_paca_global_phygtmap_terminal   import gene_Carte_paca_global_phygtmap_terminal
#from ouv_extrait_carte_europe_terminal                  import extrait_carte_europe_terminal
import subprocess

#================================================================
def Charg_europe():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_base_Europe.sh", shell=True)
    #wget http://download.geofabrik.de/openstreetmap//europe/france/provence-alpes-coted-azur-latest.osm.pbf

def MaJ_europe():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/MaJ_Europe.sh", shell=True)

def Charg_PACA():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Charg_Base_PACA.sh", shell=True)
#=====================================================================

def Courbes_phygtmap_PACA():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_phygtmap_PACA.sh", shell=True)    

def Courbes_SRTM_PACA():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_SRTM_PACA.sh", shell=True)
 
def Courbes_phygtmap_Extrait():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_phygtmap_Extrait.sh", shell=True)    

def Courbes_SRTM_Extrait():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_courbe_SRTM_Extrait.sh", shell=True)

#============================================================================
 
def Gene_Carte_PACA():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_niveau.sh", shell=True)    

def Gene_Carte_PACA():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA.sh", shell=True)
    
def Gene_Carte_PACA_global_SRTM():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_SRTM.sh", shell=True)

def Gene_Carte_PACA_global_phygtmap():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Generation_pbf_PACA_global_phygtmap.sh", shell=True)
    
 
def Extrait_carte_Europe():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/prog_variable.sh", shell=True)

def Extrait_carte_Monde():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/prog_variable_Phygtmap.sh", shell=True)

#===========================================================================================
def Compil_TYP():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/compilTYP.sh", shell=True)
#===========================================================================================
       
def About():
  subprocess.call("bash /home/michel/osm/Génération_carte/Programme/Hello.sh", shell=True)

	   
   
root = Tk()
root.title('OSM GARMIN MAP')
#root.geometry('400x100+400+400')
root.geometry('800x800+800+800')

#logo_osm=PhotoImage(file="/home/michel/osm/Génération_carte/Programme/Logo/Logo_OSM.ppm")
logo_osm=PhotoImage(file="/home/michel/osm/Génération_carte/Programme/Logo/Logo_carte.ppm")
w1 = Label(root, image=logo_osm, anchor=W)
w1.logo_osm = logo_osm
w1.pack()

menu = Menu(root)
root.config(menu=menu)

charg_menu = Menu(menu)
menu.add_cascade(label="Chargement base", menu=charg_menu)
charg_menu.add_command(label="Europe", command=Charg_europe)
charg_menu.add_command(label="PACA...", command=Charg_PACA)
charg_menu.add_separator()
charg_menu.add_command(label="Mise à jour de la base Europe...", command=Charg_PACA)
charg_menu.add_separator()
charg_menu.add_command(label="Exit", command=root.quit)

crea_courbes_menu = Menu(menu)
menu.add_cascade(label="Génération des courbes de niveaux", menu=crea_courbes_menu)
charg_menu.add_separator()
crea_courbes_menu.add_command(label="Phygtmap PACA", command=Courbes_phygtmap_PACA)
crea_courbes_menu.add_command(label="SRTM PACA", command=root.quit)
crea_courbes_menu.add_separator()
crea_courbes_menu.add_command(label="Extrait Phygtmap", command=root.quit)
crea_courbes_menu.add_command(label="Extrait SRTM", command=root.quit)
crea_courbes_menu.add_separator()
crea_courbes_menu.add_command(label="Exit", command=root.quit)

crea_menu = Menu(menu)
menu.add_cascade(label="Génération Carte GPS", menu=crea_menu)
crea_menu.add_command(label="1er Génération Carte PACA SRTM", command=Gene_Carte_PACA_global_SRTM) #Génération des courbes de niveaux avec la carte
crea_menu.add_command(label="1er Génération Carte PACA phygtmap", command=Gene_Carte_PACA_global_phygtmap)
crea_menu.add_separator()
crea_menu.add_command(label="Génération Carte PACA AVEC courbes existantes pour GPS", command=Gene_Carte_PACA)
crea_menu.add_command(label="Génération Carte PACA AVEC courbes existantes pour PC", command=Gene_Carte_PACA)
crea_menu.add_separator()
crea_menu.add_command(label="Extrait de Carte Europe", command=Extrait_carte_Europe)
crea_menu.add_command(label="Extrait de Carte Monde", command=Extrait_carte_Monde)
crea_menu.add_separator()
crea_menu.add_command(label="Exit", command=root.quit)

divers = Menu(menu)
menu.add_cascade(label="Divers", menu=divers)
divers.add_command(label="Compilation fichier.txt en TYP", command=Compil_TYP)
divers.add_separator()
divers.add_command(label="Exit", command=root.quit)


helpmenu = Menu(menu)
menu.add_cascade(label="Aide", menu=helpmenu)
helpmenu.add_command(label="A propos de ...", command=About)

mainloop()
