#!/bin/bash

#jaune='\e[1;33m'
#neutre='\e[0;m'

date="`date +%d%b%y`"

temps() { t=$(($2-$1)) ; printf "%d:%02d:%02d\n" $(($t/3600)) $((($t%3600)/60)) $(($t%60)) ; }
t1=$(date +%s)

echo " "
echo -e "${jaune}Effacement de la carte precedente ${neutre}"
echo " "


cd /home/michel/osm/Génération_carte/split_extrait_topo_10m/
rm *.*

cd /home/michel/osm/Génération_carte/split_extrait_osm/
rm *.*

cd /home/michel/osm/Génération_carte/extrait-carte/
rm *.*

###################################################################################
#echo " "
#echo -e "${jaune}Mise à jour de la base Europe ${neutre}"
#echo " " 

#zero=0
#cd /home/michel/osm/base_osm/Datasets


#osmupdate /home/michel/osm/base_osm/Datasets/maj_europe.osm.pbf /home/michel/osm/base_osm/Datasets/temp.osm.pbf -B=/home/michel/osm/base_osm/Datasets/europe.poly --max-merge=7 > osmosis.log 2>&1
#retour=$?

#if [ "$retour" -eq "$zero" ]	
#then
	#mv temp.osm.pbf maj_europe.osm.pbf
	#echo "Le traitement du fichier par osmosis a réussi" 
#else
	#echo "Le traitement du fichier par osmosis n'a pas réussi"
	#rm temp.osm.pbf
#fi
###################################################################################


#Saisie des variables
echo " "
echo -e "${jaune}Taper les coordonnées de la carte ${neutre}"
echo " "

xdg-open http://www.openstreetmap.org/export#map=6/47.130/1.830


NORD=$null
SUD=$null
EST=$null
OUEST=$null

read -p 'Coordonnées NORD :' NORD
read -p 'Coordonnées SUD :' SUD
sudPlusGrandQueNord=$(echo $SUD'>'$NORD | bc -l) # vaut 1 si vrai et 0 si faux

while (( $sudPlusGrandQueNord ))
#while test $SUD -gt $SUD
do 
	echo " "
	echo -e " ${jaune}ERREUR DE SAISIE ${neutre}" && read -p 'Coordonnées NORD :' NORD && read -p 'Coordonnées SUD :' SUD
	sudPlusGrandQueNord=$(echo $SUD'>'$NORD | bc -l) # vaut 1 si vrai et 0 si faux

done

read -p 'Coordonnées OUEST :' OUEST
read -p 'Coordonnées EST :' EST

ouestPlusGrandQueEst=$(echo $OUEST'>'$EST | bc -l) # vaut 1 si vrai et 0 si faux
while (( $ouestPlusGrandQueEst ))
do

echo -e " ${jaune}ERREUR DE SAISIE ${neutre}" && read -p 'Coordonnées OUEST :' OUEST &&	read -p 'Coordonnées EST :' EST
ouestPlusGrandQueEst=$(echo $OUEST'>'$EST | bc -l) # vaut 1 si vrai et 0 si faux

done

#heure_debut=$(date +%H%M)
#echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"

echo "Coordonnées NORD $NORD !"
echo "Coordonnées SUD $SUD !"
echo "Coordonnées OUEST $OUEST !"
echo "Coordonnées EST $EST !" 

killall firefox

################################################################
#1 Osmosis extraction zone de travail sur base europe compressée
cd /home/michel/osm/base_osm/Datasets/
osmosis --rb maj_europe.osm.pbf --bb left=$OUEST right=$EST top=$NORD bottom=$SUD --tag-filter reject-nodes internet_access=* --tag-filter reject-nodes railway=level_crossing --tag-filter reject-nodes amenity=parking --tag-filter reject-nodes parking=surface --tag-filter reject-nodes barrier=chain --tag-filter reject-nodes barrier=gate --tag-filter reject-nodes barrier=lift_gate --tag-filter reject-nodes tourism=information --tag-filter reject-nodes landuse=cemetery --wx Extract_base_extrait_europe.osm



#2 Récupération des données d'altitude
echo " "
echo -e "${jaune}Récupération des données d'altitude ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/split_extrait_topo_10m

phyghtmap -a $OUEST:$SUD:$EST:$NORD -o high_alps --source=view1,view3,srtm3 -s 10 -c 100,50 --corrx=-0.0003 --corry=0.00005 --pbf

java -Xmx2G -jar /home/michel/osm/mkgmap_dernier/mkgmap-r2714/mkgmap.jar --gmapsupp --tdbfile --family-id=44 --mapname=72500001 --family-name="Relief 10m" --draw-priority=50 --transparent --style-file=/home/michel/osm/mkgmap_dernier/mkgmap-r2714/examples/styles/default.zip --series-name=srtm --overview-mapname=srtm --area-name=PACA --max-jobs=1 *.pbf


#3 Split de la carte OSM
echo " "
echo -e "${jaune}Split de la carte OSM ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/split_extrait_osm
java -Xmx2G -jar /home/michel/osm/applis/splitter-r307/splitter.jar /home/michel/osm/base_osm/Datasets/Extract_base_extrait_europe.osm

#7 Création des 2 jeux de cartes osm et routage
echo " "
echo -e "${jaune}Création des 2 jeux de cartes osm et routage ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/split_extrait_osm
sed 's/mapname: 6324/mapname: 6100/g' template.args > routage.args
#fait

#copie des tuiles OSM et routage dans le réperoire Global
cd /home/michel/osm/Génération_carte/split_extrait_osm
cp *.* /home/michel/osm/Génération_carte/extrait_carte

#copie des tuiles topo dans le réperoire Global
cd /home/michel/osm/Génération_carte/split_extrait_topo_10m
cp *.* /home/michel/osm/Génération_carte/extrait_carte

#8 Génération de la carte Garmin couche OSM
echo " "
echo -e "${jaune}Génération de la carte Garmin couche OSM ${neutre}"
echo " "
cd /home/michel/osm/Génération_carte/extrait_carte
java -Xmx4G -jar /home/michel/osm/mkgmap_dernier/mkgmap-r2714/mkgmap.jar --gmapsupp --tdbfile --product-version="02 01 15" --series-name="02 01 15" --area-name="$date" --family-id=6324 --family-name="Fond carte OSM" --draw-priority=25 --add-pois-to-areas --style-file=/home/michel/osm/Génération_carte/style/hc-map14/hc-perso150103.zip --remove-short-arcs --max-jobs=2 --latin1 --country-name="PACA" --country-abbr="FR" --generate-sea=extend-sea-sectors -c template.args /home/michel/osm/Génération_carte/TYP/186.typ --family-id=43 --family-name="Routage OSM" --route --draw-priority=5 --transparent --style-file=/home/michel/osm/EXEMPLES/Petrovsk/Style_Petrovsk/routage.zip --add-pois-to-areas --remove-short-arcs --max-jobs=2 --latin1 --country-name="PACA" --country-abbr="FR" -c routage.args /home/michel/osm/Génération_carte/TYP/186_43.typ --family-id=44 --mapname=72500001 --family-name="Relief 10m" --draw-priority=50 --style-file=/home/michel/osm/mkgmap_dernier/mkgmap-r2714/examples/styles/default.zip --remove-short-arcs --max-jobs=2 --latin1 --series-name="relief 10m" --description="OSM" /home/michel/osm/Génération_carte/TYP/186_44.typ *.pbf 


heure_fin=$(date +%H%M)
echo -e "${jaune}HEURE DE DEBUT : $heure_debut ${neutre}"
echo -e "${jaune}HEURE DE FIN : $heure_fin ${neutre}"
echo " "
t2=$(date +%s)
temps t1 t2
cd statistiques
temps t1 t2 >extrait_10m

echo " "
echo -e "${jaune}Fin de la génération de l'extrait de carte 10m ${neutre}"
echo -e "${jaune}Répertoire /home/michel/osm/Génération_carte/extrait_carte ${neutre}"
echo " "

echo " "

echo " Taper une touche"
read sortie
echo "SORTIE DU LOGICIEL"
